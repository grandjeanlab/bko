BKO
================
Joanes Grandjean

## Introduction

This document provides the code to replicate the figures used in my BKO
application.

First we load the relevant libaries

``` r
library(tidyverse)
library(ggpubr)
library(lubridate)
```


    Attaching package: 'lubridate'

    The following objects are masked from 'package:base':

        date, intersect, setdiff, union

``` r
library(wesanderson)
pal <- wes_palette("Zissou1", 21, type = "continuous")
```

Load the table

``` r
df<-read_tsv('response.tsv',show_col_types = FALSE)
questions<-colnames(df)

df$Timestamp<-df$Timestamp %>% mdy_hms()
df$month <- floor_date(df$Timestamp, "month") 

df1<-df

tmp<-c()
for(i in 2:10){df[[i]]<-df[[i]] %>% factor(levels=c(1,2,3,4,5))}
for(i in 10:2){tmp <-df[[i]]  %>% summary %>% unclass %>% as_tibble() %>% rename(!!colnames(df)[i]:=value) %>% bind_cols(tmp,)}

tmp<-tmp %>% rownames_to_column(var = "score")
df_content<-tmp[c(1,2:4)]%>% pivot_longer(!score, names_to = "category", values_to = "count")
df_material <- tmp[c(1,5:6)]%>% pivot_longer(!score, names_to = "category", values_to = "count")
df_speaker <-tmp[c(1,9,10)]%>% pivot_longer(!score, names_to = "category", values_to = "count")
df_speed <-tmp[c(1,8)]%>% pivot_longer(!score, names_to = "category", values_to = "count")
```

Start plotting

``` r
ggballoonplot(df_content,fill = "value",size = 15,show.label = TRUE,  shape = 23)+
   scale_fill_gradientn(colors = pal)+theme(legend.position="none")
```

![](README_files/figure-gfm/content-1.png)

``` r
ggballoonplot(df_material,fill = "value",size = 15,show.label = TRUE,  shape = 23)+
   scale_fill_gradientn(colors = pal)+theme(legend.position="none",aspect.ratio=3/5)
```

![](README_files/figure-gfm/material-1.png)

``` r
ggballoonplot(df_speaker,fill = "value",size = 15,show.label = TRUE,  shape = 23)+
   scale_fill_gradientn(colors = pal)+theme(legend.position="none")
```

![](README_files/figure-gfm/speaker-1.png)

``` r
ggballoonplot(df_speed,fill = "value",size = 15,show.label = TRUE,  shape = 23)+
   scale_fill_gradientn(colors = pal)+theme(legend.position="none",aspect.ratio=1/5)
```

![](README_files/figure-gfm/speed-1.png)

``` r
ggline(df1, x = "month", y = "The speaker engaged with the students",add = c("mean_se"))+ylim(0,5)+theme_bw()+labs(title="The speaker engaged with the students",y="score")+annotate("rect",xmin=df1$month[18],xmax=df1$month[106],ymin=0,ymax=5,fill=wes_palette("Zissou1")[1],alpha=0.2)
```

![](README_files/figure-gfm/engagemeent%20over%20time-1.png)

``` r
ggline(df1, x = "month", y = "The topic presented connects with the other classes",add = c("mean_se"))+ylim(0,5)+theme_bw()+labs(title="The topic presented connects with the other classes",y="score")+annotate("rect",xmin=df1$month[18],xmax=df1$month[106],ymin=0,ymax=5,fill=wes_palette("Zissou1")[1],alpha=0.2)
```

![](README_files/figure-gfm/connect%20over%20time-1.png)
